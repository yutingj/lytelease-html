
$(document).ready(function () {
    var x = jQuery.noConflict();
});

$(document).on('change', '#inp_purchase_price', function() {
    // Action goes here.
    var purchase_price = jQuery("#inp_purchase_price").val();
    var down_payment_percent = jQuery(".down_payment_scrl").val();
    var down_payment = (purchase_price * down_payment_percent)/100;
    var interest_rate = jQuery(".interest_rate_scrl").val();
    var mortgage_term = jQuery("#mortgage_term_yr").val();
    var annual_tax_percent = jQuery(".annual_tax_scrl").val();
    var annual_tax = (purchase_price * annual_tax_percent)/100;
    var monthly_tax = (annual_tax/12);
    var monthly_insurance = (jQuery("#annual_insurance_inp").val()/12);
    var monthly_hoa = jQuery("#monthly_hoa_inp").val();
    jQuery("#down_payment_inp").val(down_payment);
    jQuery("#annual_tax_inp").val(annual_tax);
    mortgage_calc(purchase_price, down_payment, interest_rate, mortgage_term, monthly_tax, monthly_insurance, monthly_hoa);
});




$(document).on('change', '#mortgage_term_yr', function() {
    // Action goes here.
    var purchase_price = jQuery("#inp_purchase_price").val();
    var down_payment_percent = jQuery(".down_payment_scrl").val();
    var down_payment = (purchase_price * down_payment_percent)/100;
    var interest_rate = jQuery(".interest_rate_scrl").val();
    var mortgage_term = jQuery("#mortgage_term_yr").val();
    var annual_tax_percent = jQuery(".annual_tax_scrl").val();
    var annual_tax = (purchase_price * annual_tax_percent)/100;
    var monthly_tax = (annual_tax/12);
    var monthly_insurance = (jQuery("#annual_insurance_inp").val()/12);
    var monthly_hoa = jQuery("#monthly_hoa_inp").val();
    jQuery("#down_payment_inp").val(down_payment);
    jQuery("#annual_tax_inp").val(annual_tax);
    mortgage_calc(purchase_price, down_payment, interest_rate, mortgage_term, monthly_tax, monthly_insurance, monthly_hoa);
});

function mortgage_calc(price, down, rate, term, tax, insurance, hoa) {
    // Price
    price = jQuery("#inp_purchase_price").val();

    down_payment_percent = jQuery(".down_payment_scrl").val();
    down = down_payment_percent;

    // Term
    term = jQuery("#mortgage_term_yr").val();


    pv = price;
    fv = 0;
    nper = term;
    type = 0;
    // Rate
    rate = 20/100;


    if (term == 18 || term == 24) {
        rate = 18/100;
    }

    rate = rate /12;

    var premiumMonthly = -((-fv - pv * Math.pow(1 + rate, nper)) /
        (1 + rate * type) /
        ((Math.pow(1 + rate, nper) - 1) / rate));

    annual_tax_percent = jQuery(".annual_tax_scrl").val();
    annual_tax = (price * annual_tax_percent)/100;
    tax = (annual_tax/12);
    insurance = jQuery("#annual_insurance_inp").val()/12;
    hoa = jQuery("#monthly_hoa_inp").val()==""?0:jQuery("#monthly_hoa_inp").val();

    var emmp = 0;
    changethis = roundOff(emmp,2);
    var premium = premiumMonthly * term;
    jQuery("#emmp_div_span").text("SGD " + Math.round(premiumMonthly));
    //jQuery("#pi_div_span").text((p));
    jQuery("#mtax_div_span").text(roundOff(tax,2));
    jQuery("#minsure_div_span").text(roundOff(insurance,2));
    jQuery("#hoa_div_span").text(hoa);
    jQuery("#inp_purchase_price").text("SGD " + price);
    mortgage_term_select(term,premiumMonthly,premium);
}

function mortgage_term_select(term,premiumMonthly,premium) {
    var body = document.getElementById("table");
    var tbl = document.createElement('table');
    // Reset the element
    body.innerHTML = '';
    tbl.style.width = '100%';
    tbl.setAttribute('border', '0');
    tbl.setAttribute('text-align', 'center');
    tbl.setAttribute('font-family', 'Open Sans');

    var tbdy = document.createElement('tbody');
    var tr = document.createElement('tr');
    var td = document.createElement('td');
    var rowText = document.createTextNode('Month');
    var bold = document.createElement('STRONG');
    bold.appendChild(rowText);
    tr.appendChild(td).appendChild(bold);
    var tbdy = document.createElement('tbody');
    var td = document.createElement('td');


    var rowText = document.createTextNode('Monthly Payment');
    var bold = document.createElement('STRONG');
    bold.appendChild(rowText);
    tr.appendChild(td).appendChild(bold);
    tr.setAttribute('style', 'font-family:Montserrat !important; font-weight:600');
    tbdy.appendChild(tr);
    tbdy.setAttribute('border', '0 !important');

    if (term != 6) {
        for (var i = 0; i < 6; i++) {
            var tr = document.createElement('tr');
            tr.setAttribute('border', '0px solid transparent');
            for (var j = 0; j < 2; j++) {
                if (i < 2) {
                    var td = document.createElement('td');
                    var rowText = document.createTextNode(i+1);
                    var rowText2 = document.createTextNode("SGD " + Math.round(premiumMonthly));
                    if (j == 0) {
                        td.appendChild(rowText);

                    } else {
                        td.appendChild(rowText2);
                    }
                    tr.appendChild(td);
                }

                if (i == 2 || i == 3) {
                    var td = document.createElement('td');
                    var rowText = document.createTextNode("...");
                    var rowText2 = document.createTextNode("...");
                    if (j == 0) {
                        td.appendChild(rowText);

                    } else {
                        td.appendChild(rowText2);
                    }
                    tr.appendChild(td);
                }

                if (i > 3) {
                    var td = document.createElement('td');
                    var rowText = document.createTextNode(term - 5 + i);
                    var rowText2 = document.createTextNode(("SGD " + Math.round(premiumMonthly)));
                    if (j == 0) {
                        td.appendChild(rowText);

                    } else {
                        td.appendChild(rowText2);
                    }
                    tr.appendChild(td);
                }
            }
            tbdy.appendChild(tr);
        }
    }


    if (term == 6) {
        for (var i = 0; i < 6; i++) {
            var tr = document.createElement('tr');
            tr.setAttribute('border', '0px solid transparent');
            for (var j = 0; j < 2; j++) {
                if (i != 2 && i != 3) {
                    var td = document.createElement('td');
                    var rowText = document.createTextNode(i+1);
                    var rowText2 = document.createTextNode(("SGD " + Math.round(premiumMonthly)));

                    if (j == 0) {
                        td.appendChild(rowText);

                    } else {
                        td.appendChild(rowText2);
                    }
                    tr.appendChild(td);
                }

                if (i == 2 || i == 3) {
                    var td = document.createElement('td');
                    var rowText = document.createTextNode("...");
                    var rowText2 = document.createTextNode("...");
                    if (j == 0) {
                        td.appendChild(rowText);

                    } else {
                        td.appendChild(rowText2);
                    }
                    tr.appendChild(td);
                }
            }
            tbdy.appendChild(tr);
        }
    }

    tbl.appendChild(tbdy);
    body.appendChild(tbl);
    document.getElementById("table").style.fontSize = "120%";
    document.getElementById("calc").style.maxHeight ="110%";
    document.getElementById("calc").style.height ="105%";
}





function calculateMonthlyPayments(fLoanAmount , fAPR, iTerm )
{
    return (fLoanAmount/iTerm)+((fLoanAmount/iTerm)/100*(fAPR/12*iTerm));
}

function roundOff(number, precision) {
    num = number;
    return num.toFixed(2);
}
